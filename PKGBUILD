# Maintainer: Antoni Aloy <aaloytorrens@gmail.com>
# See https://github.com/Rangi42/polished-map/blob/63c033/INSTALL.md
# Seems that polished-map needs very specific software versions
pkgname=polished-map
pkgver=4.7.1+git20220917.654db2d
pkgrel=0
_commit="654db2d385a6cfcfc5be97e3d84f5420dd4b1321"
pkgdesc='Map and tileset editor for pokecrystal, pokered, and projects based on them'
arch=("amd64")
license=("LGPL-3.0")
url="https://github.com/Rangi42/polished-map"
makedepends=("autoconf" "cmake" "libpng-dev" "libxpm-dev" "zlib1g-dev" "libx11-dev" "libxft-dev" "libxinerama-dev" "libfontconfig1-dev" "x11proto-xext-dev" "libxrender-dev" "libxfixes-dev" "libgl1-mesa-dev" "libglu1-mesa-dev" "libpulse-dev")
depends=("zlib1g" "libxpm4" "libpng16-16" "libx11-6" "libxft2" "libxinerama1" "libfontconfig1" "libxrender1" "libxfixes3" "libpulse0")
source=(
	"0001-polished-map-usr-prefix.patch"
	"0002-correct-desktop-file.patch"	
	"$pkgname-$pkgver.tar.gz::https://github.com/Rangi42/polished-map/archive/654db2d.tar.gz"
	"fltk_1.3.7.tar.gz::https://github.com/fltk/fltk/archive/refs/tags/release-1.3.7.tar.gz"
)
sha256sums=('074e46ef4fe3d50babd93dc0c83078e8f4ab4615837c823eaa0b1e3e331aaa19'
            'd9941709ce857ca6eb809762844096cb212b70ad1f8a7556108dc3357c6d0ac0'
            '33cee9d396b94168054ad4d97fe3beb36836560841827486f8c0ac545f691ab9'
            '019f65810fb0ea5acac14c852193e8f374e822e6a3034a3c80ed8676f6f3a090')

prepare() {
	cd $pkgname-$_commit

	# Apply patches
	yes | patch -p1 < $srcdir/0001-polished-map-usr-prefix.patch
	yes | patch -p1 < $srcdir/0002-correct-desktop-file.patch
}

build() {
	cd $srcdir
	# Build dependencies
	# FLTK
	cd fltk-release-1.3.7
	./autogen.sh --prefix="$PWD/.." --with-abiversion=10307
	make
	make install

	# Build main package
	# ("export PATH" is needed if bin/fltk-config is not already in your PATH)
	cd $srcdir/$pkgname-$_commit
	export PATH="../bin:$PATH"
	make
}

package() {
	cd $pkgname-$_commit
	make DESTDIR="$pkgdir" install
}


# vim: set ts=2 sw=2 et:
